package events.vandal.interactionlock


import dev.jorel.commandapi.CommandAPI
import dev.jorel.commandapi.CommandAPICommand
import events.vandal.interactionlock.commands.ToggleLockCommand
import events.vandal.interactionlock.events.InteractionEvent
import events.vandal.interactionlock.tables.Locked
import org.bukkit.entity.Player
import org.bukkit.plugin.java.JavaPlugin
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.TransactionManager
import org.jetbrains.exposed.sql.transactions.transaction

class InteractionLock : JavaPlugin() {
    override fun onEnable() {
        plugin = this

        Database.connect("jdbc:h2:./${this.dataFolder.apply { this.mkdirs() }.toPath()}/data;DB_CLOSE_DELAY=-1;", "org.h2.Driver")

        CommandAPI.onEnable(this)
        CommandAPICommand("toggle-lock").apply {
            withPermission("interactionlock.admin")
            withRequirement { it is Player }

            executesPlayer(ToggleLockCommand)
        }.register()

        transaction {
            SchemaUtils.createMissingTablesAndColumns(
                Locked
            )
        }

        server.pluginManager.registerEvents(InteractionEvent(), this)
    }

    override fun onDisable() {
        TransactionManager.currentOrNull()?.connection?.close()
        TransactionManager.resetCurrent(null)
        CommandAPI.unregister("toggle-lock")
    }

    companion object {
        lateinit var plugin: InteractionLock
    }
}